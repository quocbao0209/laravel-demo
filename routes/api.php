<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\EmployeeController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

//Get all employees
Route::get('/employees', [EmployeeController::class, 'getEmployee']);

//Get Specic employee detail
Route::get('/employee/{id}', [EmployeeController::class, 'getEmployeeById']);

//add Employee
Route::get('/addEmployee', [EmployeeController::class, 'addEmployee']);

//update Employee
Route::get('/updateEmployee/{id}', [EmployeeController::class, 'updateEmployee']);

//delete Employee
Route::get('/deleteEmployee/{id}', [EmployeeController::class, 'deleteEmployee']);
